Dev Tools
=========

Development tools used for development in CTI.

Tools
-----
Download and install to `$PATH` (usually `~/.local/bin`).

- `cas` shell script wrapper around `curl` to authenticate with CAS server

