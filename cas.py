from flask import request, redirect
import urllib.parse
import urllib.request
import functools
import json
import urllib

cas_url = 'https://cas.apiit.edu.my/cas'
cas_attrs = ['sAMAccountName', 'distinguishedName']


def login_required(f):
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        """
        Authenticate through validating service ticket using parameter 'ticket'
        Save user attributes (CAS Attributes) in the kwargs with 'cas:" prefix if the service ticket is valid
        Redirect to CAS SSO if service ticket is missing or invalid
        """
        if 'ticket' not in request.args:
            return redirect(f'{cas_url}/login?service={request.base_url}')
        ticket = request.args.get('ticket')
        query = urllib.parse.urlencode({
            'format': 'json',
            'ticket': ticket,
            'service': request.base_url
        })
        with urllib.request.urlopen(f'{cas_url}/p3/serviceValidate?{query}') as url:
            data = json.load(url).get('serviceResponse')
            if 'authenticationSuccess' not in data:
                return redirect(f'{cas_url}/login?service={request.base_url}')
            a = data['authenticationSuccess']['attributes']
            for k in a:
                if k in cas_attrs: kwargs['cas:' + k] = a[k][0]
        return f(*args, **kwargs)
    return wrapper